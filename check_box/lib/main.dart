import 'package:check_box/checkBox_widget.dart';
import 'package:check_box/checkBoxtile_widget.dart';
import 'package:check_box/dropdown_widget.dart';
import 'package:check_box/radio_widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(title: 'UI Extenion', home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('UI EXtenstion')),
      drawer: Drawer(
          child: ListView(
        children: [
          const DrawerHeader(
            child: Text('UI Menu'),
            decoration: BoxDecoration(color: Colors.blue),
          ),
          ListTile(
              title: Text('CheckBox'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CheckboxWidget()));
              }),
          ListTile(
              title: Text('CheckTile'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckBoxTileWidget()));
              }),
          ListTile(
              title: Text('DropDownTile'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DropDownWidget()));
              }),
          ListTile(
              title: Text('Radio'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RadioWidget()));
              }),
        ],
      )),
    );
  }
}
